//
//  LicenceAgreementViewController.swift
//  ColdPlay
//
//  Created by Jaspreet Singh on 17/05/21.
//

import UIKit

class LicenceAgreementViewController: UIViewController {
    
    // MARK: - Outlet
    
    @IBOutlet weak var centerImageView: RoundCornerView!
    @IBOutlet weak var licenceAgreementView: RoundCornerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        centerImageView.layer.cornerRadius = centerImageView.frame.height / 2
        centerImageView.layer.masksToBounds = true
    }
    
    // MARK: - ButtonAction
    
    @IBAction func nopeStrangerButtonAction(_ sender: Any) {
        licenceAgreementView.isHidden = true
    }
    @IBAction func yesTrustButtonAction(_ sender: Any) {
        if let loginWithPhoneNbrVC = getMainStoryboard().instantiateViewController(identifier: LoginWithPhoneViewController.className) as? LoginWithPhoneViewController {
            
            self.navigationController?.pushViewController(loginWithPhoneNbrVC, animated: true)
            
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
