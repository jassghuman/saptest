//
//  LoginWithPhoneViewController.swift
//  ColdPlay
//
//  Created by Jaspreet Singh on 17/05/21.
//

import UIKit

class LoginWithPhoneViewController: UIViewController {
    
    // MARK: - Outlet

    @IBOutlet weak var phnoneNbrTextField: UITextField!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var centerImageView: RoundCornerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        centerImageView.layer.cornerRadius = centerImageView.frame.height / 2
        centerImageView.layer.masksToBounds = true

    }
    
    // MARK: - ButtonAction
    @IBAction func nextButtonAction(_ sender: Any) {
        
        if let otpVC = getMainStoryboard().instantiateViewController(identifier: OTPViewController.className) as? OTPViewController {
            self.navigationController?.pushViewController(otpVC, animated: true)
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
