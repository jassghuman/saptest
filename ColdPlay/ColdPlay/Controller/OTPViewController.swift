//
//  OTPViewController.swift
//  ColdPlay
//
//  Created by Jaspreet Singh on 17/05/21.
//

import UIKit
import SVPinView

class OTPViewController: UIViewController {

    // MARK: - Outlet
    @IBOutlet weak var otpPinView: SVPinView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: - ButtonAction
    
    @IBAction func resendOtpButtonAction(_ sender: Any) {
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        if let userDescrptVC = getMainStoryboard().instantiateViewController(identifier: UserDescrptionViewController.className) as? UserDescrptionViewController {
            self.navigationController?.pushViewController(userDescrptVC, animated: true)
        }
    }
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
