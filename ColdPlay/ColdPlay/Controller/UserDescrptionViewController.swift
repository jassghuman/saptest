//
//  UserDescrptionViewController.swift
//  ColdPlay
//
//  Created by Jaspreet Singh on 17/05/21.
//

import UIKit

class UserDescrptionViewController: UIViewController {

    // MARK: - Outlet
    @IBOutlet weak var maleButtonOutlet: UIButton!
    @IBOutlet weak var femaleButtonOutlet: UIButton!
    @IBOutlet weak var otherButtonOutlet: UIButton!
    @IBOutlet weak var age18ButtonOutlet: UIButton!
    @IBOutlet weak var age26ButtonOutlet: UIButton!
    @IBOutlet weak var age40ButtonOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: - ButtonAction
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func laterButtonAction(_ sender: Any) {
    }
    @IBAction func doneButtonAction(_ sender: Any) {
        if let walkThroughVC = getWalkThroughStoryboard().instantiateViewController(identifier: WalkthroughViewController.className) as? WalkthroughViewController {
            self.navigationController?.pushViewController(walkThroughVC, animated: true)
        }
    }
    @IBAction func maleButtonAction(_ sender: Any) {
        maleButtonOutlet.backgroundColor = .lightGray
        femaleButtonOutlet.backgroundColor = .white
        otherButtonOutlet.backgroundColor = .white
    }
    @IBAction func femaleButtonAction(_ sender: Any) {
        maleButtonOutlet.backgroundColor = .white
        femaleButtonOutlet.backgroundColor = .lightGray
        otherButtonOutlet.backgroundColor = .white

    }
    @IBAction func otherButtonAction(_ sender: Any) {
        maleButtonOutlet.backgroundColor = .white
        femaleButtonOutlet.backgroundColor = .white
        otherButtonOutlet.backgroundColor = .lightGray

    }
    @IBAction func age18ButtonAction(_ sender: Any) {
        age18ButtonOutlet.backgroundColor = .lightGray
        age26ButtonOutlet.backgroundColor = .white
        age40ButtonOutlet.backgroundColor = .white
    }
    @IBAction func age26ButtonAction(_ sender: Any) {
        age18ButtonOutlet.backgroundColor = .white
        age26ButtonOutlet.backgroundColor = .lightGray
        age40ButtonOutlet.backgroundColor = .white

    }
    @IBAction func age40ButtonAction(_ sender: Any) {
        age18ButtonOutlet.backgroundColor = .white
        age26ButtonOutlet.backgroundColor = .white
        age40ButtonOutlet.backgroundColor = .lightGray

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
