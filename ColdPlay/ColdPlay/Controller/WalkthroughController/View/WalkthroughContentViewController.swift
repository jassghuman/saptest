//
//  WalkthroughContentViewController.swift
//  ColdPlay
//
//  Created by Jaspreet Singh on 13/05/21.
//

import UIKit

class WalkthroughContentViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var introLabel: UILabel! {
        didSet {
            introLabel.numberOfLines = 0
        }
    }
    var index: Int?
    var intro: String?
    var imagefile: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        imageView.image = UIImage(named: imagefile ?? "noImage")
        introLabel.text = intro
    }
}

