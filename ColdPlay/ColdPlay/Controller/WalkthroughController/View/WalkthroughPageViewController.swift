//
//  WalkthroughPageViewController.swift
//  ColdPlay
//
//  Created by Jaspreet Singh on 13/05/21.
//

import UIKit

class WalkthroughPageViewController: UIPageViewController {
    
    // MARK: - Outlets
    
    var pageIntro = ["ColdPlay1 is creating concerts that is actively benfical for the planet. see how you can contribute.", "ColdPlay2 is creating concerts that is actively benfical for the planet. see how you can contribute.", "ColdPlay3 is creating concerts that is actively benfical for the planet. see how you can contribute.", "ColdPlay4 is creating concerts that is actively benfical for the planet. see how you can contribute.", "ColdPlay5 is creating concerts that is actively benfical for the planet. see how you can contribute."]
    
    var pageImages = ["image1", "image1", "image1", "image1", "image1"]
    weak var walkThroughDelegate: WalkThroughPageViewControllerDelegate?
    var currentPageIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        delegate = self
        if let startingViewController = contentViewController(at: 0) {
            setViewControllers([startingViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    func contentViewController(at index: Int) -> WalkthroughContentViewController? {
        if index < 0 || index >= pageIntro.count {
            return nil
        }
        let storyboard = UIStoryboard(name: "WalkThroughStoryboard", bundle: nil)
        if let pageContentViewController = storyboard.instantiateViewController(identifier: "WalkthroughContentViewController") as? WalkthroughContentViewController {
            pageContentViewController.imagefile = pageImages[index]
            pageContentViewController.intro = pageIntro[index]
            pageContentViewController.index = index
            return pageContentViewController
        }
        return nil
    }
}

// MARK: - UIPageViewControllerDataSource

extension WalkthroughPageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate  {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = (viewController as? WalkthroughContentViewController)!.index
        walkThroughDelegate?.walkThroughPageViewController(tutorialPageViewController: self, didUpdatePageIndex: index ?? 0)
        index! -= 1
        return contentViewController(at: index!)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = (viewController as? WalkthroughContentViewController)!.index
        let currentIndex = (viewController as? WalkthroughContentViewController)!.index
        index! += 1
        walkThroughDelegate?.walkThroughPageViewController(tutorialPageViewController: self, didUpdatePageIndex: currentIndex ?? 0)
        return contentViewController(at: index!)
    }
}

// MARK: - Protocol WalkThroughPageViewController

protocol WalkThroughPageViewControllerDelegate: class {
    
    func walkThroughPageViewController(tutorialPageViewController: WalkthroughPageViewController, didUpdatePageCount count: Int)
    
    func walkThroughPageViewController(tutorialPageViewController: WalkthroughPageViewController, didUpdatePageIndex index: Int)
    
}
