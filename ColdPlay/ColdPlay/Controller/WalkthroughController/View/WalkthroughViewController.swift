//
//  WalkthroughViewController.swift
//  ColdPlay
//
//  Created by Jaspreet Singh on 13/05/21.
//

import UIKit

class WalkthroughViewController: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    var walkthroughPageViewController: WalkthroughPageViewController? {
        didSet {
            walkthroughPageViewController?.walkThroughDelegate = self
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func logInButtonAction(_ sender: Any) { }
    
    @IBAction func getStartedButtonAction(_ sender: Any) { }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        let destination = segue.destination
        if let pageviewController = destination as? WalkthroughPageViewController {
            walkthroughPageViewController = pageviewController
        }
    }
}

// MARK: - ProtocolMethodWalkThroughPageViewController

extension WalkthroughViewController: WalkThroughPageViewControllerDelegate {
    
    func walkThroughPageViewController(tutorialPageViewController: WalkthroughPageViewController, didUpdatePageCount count: Int) {
        pageControl.currentPage = 5
    }
    
    func walkThroughPageViewController(tutorialPageViewController: WalkthroughPageViewController, didUpdatePageIndex index: Int) {
        pageControl.currentPage = index
    }
}
