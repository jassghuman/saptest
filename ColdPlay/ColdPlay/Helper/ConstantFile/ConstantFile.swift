//
//  File.swift
//  ColdPlay
//
//  Created by Jaspreet Singh on 17/05/21.
//

import Foundation
import UIKit

extension NSObject {
    
    class var className: String {
        return String(describing: self)
    }
}

func getWalkThroughStoryboard() -> UIStoryboard {
    return UIStoryboard(name: "WalkThroughStoryboard", bundle: nil)
}

func getMainStoryboard() -> UIStoryboard {
    return UIStoryboard(name: "Main", bundle: nil)
}

