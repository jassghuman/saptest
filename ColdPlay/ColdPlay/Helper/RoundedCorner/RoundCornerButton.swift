//
//  RoundCornerButton.swift
//  ColdPlay
//
//  Created by Jaspreet Singh on 10/05/21.
//

import UIKit

@IBDesignable open class RoundCornerButton: UIButton {
    
    @IBInspectable open var cornerRadiusB: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadiusB
        }
    }
    
    @IBInspectable var borderWidthB: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidthB
        }
    }
    @IBInspectable var borderColorB: UIColor? {
        didSet {
            layer.borderColor = borderColorB?.cgColor
        }
    }
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.*/
    override open func draw(_ rect: CGRect) {
        
        // Drawing code
//        if(cornerRadius == 0.0){
//            self.layer.cornerRadius = self.frame.height/2
//            self.layer.masksToBounds = true
//        }
//        else{
            self.layer.cornerRadius = cornerRadiusB
            self.layer.masksToBounds = true
            
//        }
        self.layer.borderColor = borderColorB?.cgColor
        self.layer.borderWidth = borderWidthB
    }
}

